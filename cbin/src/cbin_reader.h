
#ifndef CBIN_READER_H
#define CBIN_READER_H
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "cbin_helper.h"



typedef struct
{
	const void* _buffer;
	size_t _size;
	size_t _position;
    cbin_error_t _error;
} cbin_reader_t;

/// \brief Initializes the reader.
/// \param reader The reader to initialize.
/// \param buffer The buffer to read from.
/// \param size The size of the buffer.
CBIN_API void cbin_reader_init(cbin_reader_t* reader, const void* buffer, size_t size);

/// \brief Resets the reader to the beginning.
/// \param reader The reader to reset.
CBIN_API void cbin_reader_reset(cbin_reader_t* reader);

/// \brief Skips the specified number of bytes.
/// \param reader The reader to use.
/// \param size The number of bytes to skip.
/// \returns true if the reader was able to skip the specified number of bytes, false otherwise.
CBIN_API void cbin_reader_skip(cbin_reader_t* reader, size_t size);

/// \brief Gets the buffer the reader is reading from.
/// \param reader The reader to get the buffer from.
/// \return The buffer the reader is reading from.
CBIN_API const void* cbin_reader_get_buffer(const cbin_reader_t* reader);

/// \brief Gets the size of the buffer the reader is reading from.
/// \param reader The reader to get the size of the buffer from.
/// \return The size of the buffer the reader is reading from.
CBIN_API size_t cbin_reader_get_size(const cbin_reader_t* reader);


/// \brief Gets the current position of the reader.
/// \param reader The reader to get the position of.
/// \return The current position of the reader.
CBIN_API size_t cbin_reader_get_position(const cbin_reader_t* reader);


/// \brief Sets the position of the reader.
/// \param reader The reader to set the position of.
/// \param position The position to set the reader to.
CBIN_API void cbin_reader_set_position(cbin_reader_t* reader, size_t position);


/// \brief Gets the last error.
/// \param reader The reader to get the last error from.
/// \return The last error.
CBIN_API cbin_error_t cbin_reader_get_error(const cbin_reader_t* reader);

/// \brief Reads raw data from the reader.
/// \param reader The reader to read from.
/// \param size The number of bytes to read.
/// \param result The result of the read.
CBIN_API void cbin_read_raw(cbin_reader_t* reader, size_t size, void* result);

/// \brief Reads a LEB128 prefixed blob of bytes.
/// \param reader The reader to read from.
/// \param result The result of the read.
/// \param capacity The capacity of the result.
CBIN_API void cbin_read_blob(cbin_reader_t* reader, void* result, size_t capacity);

/// \brief Reads an 8-bits signed integer.
/// \param reader The reader to read from.
CBIN_API int8_t cbin_read_int8(cbin_reader_t* reader);

/// \brief Reads an 8-bits unsigned integer.
/// \param reader The reader to read from.
CBIN_API uint8_t cbin_read_uint8(cbin_reader_t* reader);

/// \brief Reads a 16-bits signed integer.
/// \param reader The reader to read from.
CBIN_API int16_t cbin_read_int16(cbin_reader_t* reader);

/// \brief Reads a 16-bits unsigned integer.
/// \param reader The reader to read from.
CBIN_API uint16_t cbin_read_uint16(cbin_reader_t* reader);

/// \brief Reads a 32-bits signed integer.
/// \param reader The reader to read from.
CBIN_API int32_t cbin_read_int32(cbin_reader_t* reader);

/// \brief Reads a 32-bits unsigned integer.
/// \param reader The reader to read from.
CBIN_API uint32_t cbin_read_uint32(cbin_reader_t* reader);

/// \brief Reads a 64-bits signed integer.
/// \param reader The reader to read from.
CBIN_API int64_t cbin_read_int64(cbin_reader_t* reader);

/// \brief Reads a 64-bits unsigned integer.
/// \param reader The reader to read from.
CBIN_API uint64_t cbin_read_uint64(cbin_reader_t* reader);

/// \brief Reads a 32-bits floating point number, encoded as a 32-bits unsigned integer.
/// \param reader The reader to read from.
CBIN_API float cbin_read_float(cbin_reader_t* reader);

/// \brief Reads a 64-bits floating point number, encoded as a 64-bits unsigned integer.
/// \param reader The reader to read from.
CBIN_API double cbin_read_double(cbin_reader_t* reader);

/// \brief Reads a 64-bits unsigned integer, encoded as unsigned LEB128
/// \param reader The reader to read from.
/// \param len The length of the integer.
CBIN_API uint64_t cbin_read_leb128(cbin_reader_t* reader, size_t *len);

/// \brief Reads a LEB128 prefixed and null-terminated string.
/// \param reader The reader to read from.
/// \param result The result of the read.
/// \returns The string read.
CBIN_API const char* cbin_read_string(cbin_reader_t* reader, size_t* length);


inline static bool cbin_reader_has_error(const cbin_reader_t *reader) {
    return reader->_error.error != CBIN_ERROR_NONE;
}


#endif // CBIN_READER_H
