//
// Created by Slayer on 09/07/2022.
//
#include "cbin_helper.h"

void *cbin_default_malloc(size_t size,
                          void *user_data) {
  return malloc(size);
}

void *cbin_default_realloc(void *ptr, size_t new_size,
                           void *user_data) {
  return realloc(ptr, new_size);
}

void cbin_default_free(void *ptr, void *user_data) {
  free(ptr);
}
bool cbin_print_error(cbin_error_t error, FILE *out) {
  switch (error.error) {

  case CBIN_ERROR_NONE:
    return false;
  case CBIN_ERROR_OUT_OF_MEMORY:
    fprintf(out, "CBIN_ERROR: Out of Memory at %zu\n", error.position);
    break;
  case CBIN_ERROR_END_OF_BUFFER:
    fprintf(out, "CBIN_ERROR: End of Buffer at %zu\n", error.position);
    break;
  case CBIN_ERROR_INVALID_DATA:
    fprintf(out, "CBIN_ERROR: Invalid Data at %zu\n", error.position);
    break;
  }
  fflush(out);
  return true;
}
