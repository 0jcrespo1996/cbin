#include "cbin_writer.h"
#include "cbin_helper.h"

#include <assert.h>
#include <string.h>

#if defined(CBIN_BREAK_ON_ERROR)
#define CBIN_CHECK_ERROR(writer)                                               \
  do {                                                                         \
    if (cbin_print_error(cbin_writer_get_error((writer)), stderr)) {           \
      CBIN_BREAK();                                                            \
    }                                                                          \
  } while (0)
#else
#define CBIN_CHECK_ERROR(writer) ((void)0)
#endif

void cbin_writer_init(cbin_writer_t *writer, cbin_malloc_t malloc,
                      cbin_realloc_t realloc, cbin_free_t free,
                      void *user_data) {
  writer->_position = 0;
  writer->_capacity = 0;
  writer->_buffer = NULL;
  writer->_written = 0;
  writer->_error = (cbin_error_t){.error = CBIN_ERROR_NONE, .position = 0};
  writer->_malloc = malloc;
  writer->_realloc = realloc;
  writer->_free = free;
  writer->_user_data = user_data;
}

void cbin_writer_init_default(cbin_writer_t *writer) {
  cbin_writer_init(writer, cbin_default_malloc, cbin_default_realloc,
                   cbin_default_free, NULL);
}

void cbin_writer_destroy(cbin_writer_t *writer) {
  if (writer->_free)
    writer->_free(writer->_buffer, writer->_user_data);
  memset(writer, 0, sizeof(cbin_writer_t));
}

size_t cbin_writer_get_written(const cbin_writer_t *writer) {
  return writer->_written;
}

const void *cbin_writer_get_buffer(const cbin_writer_t *writer, size_t *size) {
  if (size != NULL)
    *size = writer->_written;
  return writer->_buffer;
}

cbin_error_t cbin_writer_get_error(const cbin_writer_t *writer) {
  return writer->_error;
}

void cbin_writer_reset(cbin_writer_t *writer) {
  writer->_position = 0;
  writer->_written = 0;
  writer->_error = (cbin_error_t){.error = CBIN_ERROR_NONE, .position = 0};
}

size_t cbin_writer_get_position(const cbin_writer_t *writer) {
  return writer->_position;
}

void cbin_writer_set_position(cbin_writer_t *writer, const size_t position) {
  CBIN_CHECK_ERROR(writer);
  if (cbin_writer_has_error(writer))
    return;
  if (position > writer->_capacity) {
    writer->_error =
        (cbin_error_t){.error = CBIN_ERROR_END_OF_BUFFER, .position = position};
    CBIN_CHECK_ERROR(writer);
    return;
  }
  writer->_position = position;
}

bool cbin_writer_reserve(cbin_writer_t *writer, const size_t size) {
  CBIN_CHECK_ERROR(writer);
  if (cbin_writer_has_error(writer))
    return false;
  if (size == 0)
    return true;
  if (writer->_capacity >= writer->_position + size)
    return true;
  size_t new_capacity = (writer->_capacity + size) * 2;
  void *buffer = NULL;
  if (writer->_realloc == NULL) {
    if (writer->_malloc == NULL)
      return false;
    buffer = writer->_malloc(new_capacity, writer->_user_data);
    if (buffer == NULL) {
      new_capacity /= 2;
      buffer = writer->_malloc(new_capacity, writer->_user_data);
    }
    if (buffer != NULL)
      memmove(buffer, writer->_buffer, writer->_written);
    if (writer->_free != NULL)
      writer->_free(writer->_buffer, writer->_user_data);
  } else {
    buffer =
        writer->_realloc(writer->_buffer, new_capacity, writer->_user_data);
  }
  if (buffer == NULL)
    return false;
  writer->_buffer = buffer;
  writer->_capacity = new_capacity;
  return true;
}

void *cbin_apply_write(cbin_writer_t *writer, size_t size) {
  if (!cbin_writer_reserve(writer, size)) {
    writer->_error = (cbin_error_t){.error = CBIN_ERROR_OUT_OF_MEMORY,
                                    .position = writer->_position};
    CBIN_CHECK_ERROR(writer);
    return NULL;
  }
  if (writer->_position + size > writer->_written)
    writer->_written = writer->_position + size;
  void *buffer = (char *)writer->_buffer + writer->_position;
  writer->_position += size;
  return buffer;
}

void cbin_write_raw(cbin_writer_t *writer, const void *data,
                    const size_t size) {
  void *buffer = cbin_apply_write(writer, size);
  if (buffer == NULL)
    return;
  memcpy(buffer, data, size);
}

void cbin_write_blob(cbin_writer_t *writer, const void *data,
                     const size_t size) {
  cbin_write_leb128(writer, (int64_t)size);
  cbin_write_raw(writer, data, size);
}

void cbin_write_int8(cbin_writer_t *writer, const int8_t value) {
  cbin_write_raw(writer, &value, sizeof(int8_t));
}

void cbin_write_uint8(cbin_writer_t *writer, const uint8_t value) {
  cbin_write_int8(writer, (int8_t)value);
}

void cbin_write_int16(cbin_writer_t *writer, int16_t value) {
  value = CBIN_INT16_TO_TARGET_ENDIANNESS(value);
  cbin_write_raw(writer, &value, sizeof(int16_t));
}

void cbin_write_uint16(cbin_writer_t *writer, const uint16_t value) {
  cbin_write_int16(writer, (int16_t)value);
}

void cbin_write_int32(cbin_writer_t *writer, int32_t value) {
  value = CBIN_INT32_TO_TARGET_ENDIANNESS(value);
  cbin_write_raw(writer, &value, sizeof(int32_t));
}

void cbin_write_uint32(cbin_writer_t *writer, const uint32_t value) {
  cbin_write_int32(writer, (int32_t)value);
}

void cbin_write_int64(cbin_writer_t *writer, int64_t value) {
  value = CBIN_INT64_TO_TARGET_ENDIANNESS(value);
  cbin_write_raw(writer, &value, sizeof(int64_t));
}

void cbin_write_uint64(cbin_writer_t *writer, const uint64_t value) {
  cbin_write_int64(writer, (int64_t)value);
}

void cbin_write_float(cbin_writer_t *writer, float value) {
  static_assert(sizeof(float) == sizeof(uint32_t), "float size mismatch");
  uint32_t *bits = (uint32_t *)&value;
  cbin_write_uint32(writer, *bits);
}

void cbin_write_double(cbin_writer_t *writer, double value) {
  static_assert(sizeof(double) == sizeof(uint64_t), "double size mismatch");
  uint64_t *bits = (uint64_t *)&value;
  cbin_write_uint64(writer, *bits);
}

void cbin_write_bool(cbin_writer_t *writer, const int value) {
  cbin_write_int8(writer, value ? 1 : 0);
}

void cbin_write_leb128(cbin_writer_t *writer, const uint64_t value) {
  uint8_t buffer[10] = {0};
  const size_t size = cbin_encode_leb128(value, buffer);
  cbin_write_raw(writer, buffer, size);
}

void cbin_write_string(cbin_writer_t *writer, const char *value) {
  const size_t len = strlen(value);
  cbin_write_leb128(writer, (uint64_t)len);
  cbin_write_raw(writer, value, len);
#ifdef CBIN_NULL_TERMINATED_STRINGS
  cbin_write_int8(writer, 0);
#endif
}
