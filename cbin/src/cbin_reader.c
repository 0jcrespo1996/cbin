#include "cbin_reader.h"
#include "cbin_helper.h"
#include <assert.h>
#include <string.h>

#if defined(CBIN_BREAK_ON_ERROR)
#define CBIN_CHECK_ERROR(reader)                                               \
  do {                                                                         \
    if (cbin_print_error(cbin_reader_get_error((reader)), stderr)) {           \
      CBIN_BREAK();                                                            \
    }                                                                          \
  } while (0)
#else
#define CBIN_CHECK_ERROR(reader) ((void)0)
#endif

const void *cbin_reader_get_current_buffer(cbin_reader_t *reader) {
  return (char*)reader->_buffer + reader->_position;
}

void cbin_reader_init(cbin_reader_t *reader, const void *buffer, size_t size) {
  reader->_buffer = (const uint8_t *)buffer;
  reader->_position = 0;
  reader->_size = size;
  reader->_error = (cbin_error_t){.error = CBIN_ERROR_NONE, .position = 0};
  assert(buffer != NULL || size == 0);
}

void cbin_reader_reset(cbin_reader_t *reader) {
  reader->_position = 0;
  reader->_error = (cbin_error_t){.error = CBIN_ERROR_NONE, .position = 0};
}

void cbin_reader_skip(cbin_reader_t *reader, size_t size) {
  CBIN_CHECK_ERROR(reader);
  if (cbin_reader_has_error(reader))
    return;
  if (reader->_position + size > reader->_size) {
    reader->_position = reader->_size;
  }
  reader->_position += size;
}

const void *cbin_reader_get_buffer(const cbin_reader_t *reader) {
  return reader->_buffer;
}

size_t cbin_reader_get_size(const cbin_reader_t *reader) {
  return reader->_size;
}

size_t cbin_reader_get_position(const cbin_reader_t *reader) {
  return reader->_position;
}

void cbin_reader_set_position(cbin_reader_t *reader, const size_t position) {
  if (position > reader->_size) {
    reader->_error =
        (cbin_error_t){.error = CBIN_ERROR_END_OF_BUFFER, .position = position};
  }
  CBIN_CHECK_ERROR(reader);
  reader->_position = position;
}

cbin_error_t cbin_reader_get_error(const cbin_reader_t *reader) {
  return reader->_error;
}

void cbin_read_raw(cbin_reader_t *reader, const size_t size, void *result) {
  CBIN_CHECK_ERROR(reader);
  if (cbin_reader_has_error(reader))
    return;
  if (reader->_position + size > reader->_size) {
    reader->_error = (cbin_error_t){.error = CBIN_ERROR_END_OF_BUFFER,
                                    .position = reader->_position + size};
    CBIN_CHECK_ERROR(reader);
    return;
  }
  const char *ptr = (const char *)cbin_reader_get_current_buffer(reader);
  memcpy(result, ptr, size);
  reader->_position += size;
}

void cbin_read_blob(cbin_reader_t *reader, void *result, size_t capacity) {
  uint64_t len = cbin_read_leb128(reader, NULL);
  size_t to_skip = len > capacity ? len - capacity : 0;
  cbin_read_raw(reader, cbin_min(len, capacity), result);
  cbin_reader_skip(reader, to_skip);
}

int8_t cbin_read_int8(cbin_reader_t *reader) {
  uint8_t v[sizeof(int8_t)] = {0};
  cbin_read_raw(reader, sizeof v, v);
  return *(int8_t *)v;
}

uint8_t cbin_read_uint8(cbin_reader_t *reader) {
  return (uint8_t)cbin_read_int8(reader);
}

int16_t cbin_read_int16(cbin_reader_t *reader) {
  uint8_t v[sizeof(int16_t)];
  cbin_read_raw(reader, sizeof v, v);
  int16_t result = *(const int16_t *)v;
  result = CBIN_INT16_TO_TARGET_ENDIANNESS(result);
  return result;
}

uint16_t cbin_read_uint16(cbin_reader_t *reader) {
  return (uint16_t)cbin_read_int16(reader);
}

int32_t cbin_read_int32(cbin_reader_t *reader) {
  uint8_t v[sizeof(int32_t)];
  cbin_read_raw(reader, sizeof v, v);
  int32_t result = *(const int32_t *)v;
  result = CBIN_INT32_TO_TARGET_ENDIANNESS(result);
  return result;
}

uint32_t cbin_read_uint32(cbin_reader_t *reader) {
  return (uint32_t)cbin_read_int32(reader);
}

int64_t cbin_read_int64(cbin_reader_t *reader) {
  uint8_t v[sizeof(int64_t)];
  cbin_read_raw(reader, sizeof v, v);
  int64_t result = *(const int64_t *)v;
  result = CBIN_INT64_TO_TARGET_ENDIANNESS(result);
  return result;
}

uint64_t cbin_read_uint64(cbin_reader_t *reader) {
  return (uint64_t)cbin_read_int64(reader);
}

float cbin_read_float(cbin_reader_t *reader) {
  static_assert(sizeof(float) == sizeof(uint32_t), "float size mismatch");
  uint32_t bits = cbin_read_uint32(reader);
  return *(const float *)&bits;
}

double cbin_read_double(cbin_reader_t *reader) {
  static_assert(sizeof(double) == sizeof(uint64_t), "double size mismatch");
  uint64_t bits = cbin_read_uint64(reader);
  return *(const double *)&bits;
}

uint64_t cbin_read_leb128(cbin_reader_t *reader, size_t *len) {
  size_t length = 0;
  uint64_t result = 0;
  size_t max = reader->_size - reader->_position;
  if (max == 0) {
    reader->_error = (cbin_error_t){.error = CBIN_ERROR_END_OF_BUFFER,
                                    .position = reader->_position};
    CBIN_CHECK_ERROR(reader);
    return 0;
  }
  if (!cbin_decode_leb128((uint8_t *)cbin_reader_get_current_buffer(reader),
                          &length, &result, max)) {
    reader->_error = (cbin_error_t){.error = CBIN_ERROR_INVALID_DATA,
                                    .position = reader->_position};
    CBIN_CHECK_ERROR(reader);
    return 0;
  }
  reader->_position += length;
  if (len)
    *len = length;
  return result;
}

const char *cbin_read_string(cbin_reader_t *reader, size_t *length) {
  size_t len = cbin_read_leb128(reader, NULL);
  const char *str = cbin_reader_get_current_buffer(reader);
  cbin_reader_skip(reader, len);
  if (length != NULL)
    *length = len;

#ifdef CBIN_NULL_TERMINATED_STRINGS
  cbin_reader_skip(reader, sizeof(int8_t));
#endif
  return str;
}
