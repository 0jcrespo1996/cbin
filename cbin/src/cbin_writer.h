#ifndef CBIN_WRITER_H
#define CBIN_WRITER_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "cbin_helper.h"




typedef struct {
    uint8_t *_buffer;
    size_t _capacity;
    size_t _position;
    size_t _written;
    cbin_error_t _error;

    cbin_malloc_t _malloc;
    cbin_realloc_t _realloc;
    cbin_free_t _free;

    void* _user_data;
} cbin_writer_t;

/// \brief Initializes the writer.
/// \param writer The writer to initialize.
/// \param malloc The function to allocate memory
/// \param realloc The function to reallocate memory (if NULL, malloc and free will be used)
/// \param free The function to free memory (NULL means no freeing)
/// \param user_data The user data to pass to the malloc and free functions.
CBIN_API void cbin_writer_init(cbin_writer_t *writer, cbin_malloc_t malloc, cbin_realloc_t realloc, cbin_free_t free, void* user_data);

/// \brief Initializes the writer with the default malloc/realloc/free functions.
/// \param writer The writer to initialize.
CBIN_API void cbin_writer_init_default(cbin_writer_t* writer);

/// \brief Destroys a writer.
/// \param writer The writer to destroy.
CBIN_API void cbin_writer_destroy(cbin_writer_t *writer);

/// \brief Gets the number of bytes written to the writer.
/// \param writer The writer to get the number of bytes written to.
/// \return  The number of bytes written to the writer.
CBIN_API size_t cbin_writer_get_written(const cbin_writer_t *writer);


/// \brief Gets the buffer written to.
/// \param writer The writer to get the buffer written to.
/// \param size The size of the buffer written to.
/// \return The buffer written to.
CBIN_API const void *cbin_writer_get_buffer(const cbin_writer_t *writer, size_t *size);


/// \brief Gets the last error.
/// \param writer The writer to get the last error from.
/// \return The last error.
CBIN_API cbin_error_t cbin_writer_get_error(const cbin_writer_t *writer);


/// \brief Resets the the position and written bytes.
/// \param writer The writer to reset.
CBIN_API void cbin_writer_reset(cbin_writer_t *writer);


/// \brief Gets the current position in the writer.
/// \param writer The writer to get the position of.
/// \return The current position in the writer.
CBIN_API size_t cbin_writer_get_position(const cbin_writer_t *writer);

/// \brief Sets the current position in the writer.
/// \param writer The writer to set the position of.
/// \param position The position to set.
CBIN_API void cbin_writer_set_position(cbin_writer_t *writer, size_t position);

/// \brief Reserves bytes in the writer.
/// \param writer The writer to reserve bytes in.
/// \param size The number of bytes to reserve.
/// \return True if the bytes were reserved, otherwise false.
CBIN_API bool cbin_writer_reserve(cbin_writer_t *writer, size_t size);

/// \brief Writes bytes to the writer.
/// \param writer The writer to write bytes to.
/// \param data The data to write.
/// \param size The number of bytes to write.
CBIN_API void cbin_write_raw(cbin_writer_t *writer, const void *data, size_t size);

/// \brief Writes a LEB128 prefixed blob of bytes
/// \param writer The writer to write bytes to.
/// \param data The data to write.
/// \param size The number of bytes to write.
CBIN_API void cbin_write_blob(cbin_writer_t *writer, const void *data, size_t size);

/// \brief Writes an 8-bit signed integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_int8(cbin_writer_t *writer, int8_t value);

/// \brief Writes a 8-bit unsigned integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_uint8(cbin_writer_t *writer, uint8_t value);

/// \brief Writes a 16-bit signed integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_int16(cbin_writer_t *writer, int16_t value);

/// \brief Writes a 16-bit unsigned integer.
/// \param writer The writer to write bytes to.
/// \param value  The value to write.
CBIN_API void cbin_write_uint16(cbin_writer_t *writer, uint16_t value);

/// \brief Writes a 32-bit signed integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_int32(cbin_writer_t *writer, int32_t value);


/// \brief Writes a 32-bit unsigned integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_uint32(cbin_writer_t *writer, uint32_t value);

/// \brief Writes a 64-bit signed integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_int64(cbin_writer_t *writer, int64_t value);

/// \brief Writes a 64-bit unsigned integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_uint64(cbin_writer_t *writer, uint64_t value);

/// \brief Writes a 32-bit float, encoded as a 32-bit unsigned integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_float(cbin_writer_t *writer, float value);

/// \brief Writes a 64-bit double, encoded as a 64-bit unsigned integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_double(cbin_writer_t *writer, double value);

/// \brief Writes a boolean value as a single byte.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_bool(cbin_writer_t *writer, int value);

/// \brief Writes a LEB128 encoded 64-bits unsigned integer.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_leb128(cbin_writer_t *writer, uint64_t value);

/// \brief Writes a LEB128 prefixed and null-terminated string.
/// \param writer The writer to write bytes to.
/// \param value The value to write.
CBIN_API void cbin_write_string(cbin_writer_t *writer, const char *value);


inline static bool cbin_writer_has_error(const cbin_writer_t *writer) {
    return writer->_error.error != CBIN_ERROR_NONE;
}


#endif // CBIN_WRITER_H
