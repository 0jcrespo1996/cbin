#ifndef CBIN_HELPER_H
#define CBIN_HELPER_H
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include "endianness.h"


#if defined(CBIN_BIG_ENDIAN)
#if defined(__BIG_ENDIAN__)
#define CBIN_IS_TARGET_ENDIANNESS() (true)
#else
#define CBIN_IS_TARGET_ENDIANNESS() (false)
#endif
#define CBIN_INT16_TO_TARGET_ENDIANNESS(v) (hton16(v))
#define CBIN_INT32_TO_TARGET_ENDIANNESS(v) (hton32(v))
#define CBIN_INT64_TO_TARGET_ENDIANNESS(v) (hton64(v))
#define CBIN_FLOAT_TO_TARGET_ENDIANNESS(v) (htonf(v))
#define CBIN_DOUBLE_TO_TARGET_ENDIANNESS(v) (htond(v))
#else
#if defined(__LITTLE_ENDIAN__)
#define CBIN_IS_TARGET_ENDIANNESS() (true)
#define CBIN_INT16_TO_TARGET_ENDIANNESS(v) (v)
#define CBIN_INT32_TO_TARGET_ENDIANNESS(v) (v)
#define CBIN_INT64_TO_TARGET_ENDIANNESS(v) (v)
#define CBIN_FLOAT_TO_TARGET_ENDIANNESS(v) (v)
#define CBIN_DOUBLE_TO_TARGET_ENDIANNESS(v) (v)
#else
#define CBIN_IS_TARGET_ENDIANNESS() (false)
#define CBIN_INT16_TO_TARGET_ENDIANNESS(v) (bswap16(v))
#define CBIN_INT32_TO_TARGET_ENDIANNESS(v) (bswap32(v))
#define CBIN_INT64_TO_TARGET_ENDIANNESS(v) (bswap64(v))
#define CBIN_FLOAT_TO_TARGET_ENDIANNESS(v) (bswapf(v))
#define CBIN_DOUBLE_TO_TARGET_ENDIANNESS(v) (bswapd(v))
#endif
#endif


inline static size_t cbin_encode_leb128(uint64_t value, uint8_t* p)
{
    uint8_t* start = p;
    do {
        uint8_t byte = value & 0x7f;
        value >>= 7;
        if (value != 0) {
            byte |= 0x80;
        }
        *p++ = byte;
    } while (value != 0);
    return p - start;
}

inline static bool cbin_decode_leb128(const uint8_t* p, size_t* i, uint64_t* result, size_t max_count)
{
    const uint8_t* start = p;
    uint64_t value = 0;
    uint8_t shift = 0;
    while (true) {
        if ((size_t)(p - start) >= max_count) {
            return false;
        }
        uint8_t byte = *p++;
        value |= (byte & 0x7f) << shift;
        shift += 7;
        if ((byte & 0x80) == 0) {
            break;
        }
    }
    *result = value;
    *i = p - start;
    return true;
}

#if defined(__cplusplus)
#define CBIN_API extern "C"
#else
#define CBIN_API extern
#endif

typedef void*(*cbin_malloc_t)(size_t size, void* user_data);
typedef void*(*cbin_realloc_t)(void* ptr, size_t new_size, void* user_data);
typedef void(*cbin_free_t)(void* ptr, void* user_data);

typedef enum {
    CBIN_ERROR_NONE = 0,
    CBIN_ERROR_OUT_OF_MEMORY,
    CBIN_ERROR_END_OF_BUFFER,
    CBIN_ERROR_INVALID_DATA,
} cbin_error_e;

typedef struct {
    cbin_error_e error;
    size_t position;
} cbin_error_t;


CBIN_API void* cbin_default_malloc(size_t size, void* user_data);
CBIN_API void* cbin_default_realloc(void* ptr, size_t new_size, void* user_data);
CBIN_API void cbin_default_free(void* ptr, void* user_data);

CBIN_API bool cbin_print_error(cbin_error_t error, FILE* out);


#if defined(CBIN_BREAK_ON_ERROR)
#if defined(_WIN32)
#define CBIN_BREAK() __debugbreak()
#elif defined(__linux__) || defined(_unix) || defined(__unix) || defined(__unix__)
#include <signal.h>
#define CBIN_BREAK() raise(SIGTRAP)
#elif defined(__APPLE__) || defined(__MACH__)
#include <signal.h>
#define CBIN_BREAK() raise(SIGTRAP)
#else
#define CBIN_BREAK() ((void)0)
#endif
#endif


#ifdef max
#define cbin_max(a, b) max(a, b)
#else
#define cbin_max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifdef min
#define cbin_min(a, b) min(a, b)
#else
#define cbin_min(a,b) (((a) < (b)) ? (a) : (b))
#endif


#endif // CBIN_HELPER_H
