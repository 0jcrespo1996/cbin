//
// Created by Slayer on 10/07/2022.
//

#include <gtest/gtest.h>
#include <array>
#include <cbin_writer.h>
#include <cbin_reader.h>
#include <string>
#include <string_view>

struct BaseAllocator {
    virtual ~BaseAllocator() = default;

    virtual void *Allocate(size_t size) = 0;

    virtual void *Reallocate(void *ptr, size_t size) = 0;

    virtual void Free(void *ptr) = 0;
};

template<size_t Size = 512>
struct StackAllocator : BaseAllocator {
    std::array<uint8_t, Size> Data{};
    size_t Allocated{0};

    StackAllocator() = default;

    void *Allocate(size_t size) override {
        if (Allocated + size > Size) {
            return nullptr;
        }
        void *result = &Data[Allocated];
        Allocated += size;
        return result;
    }

    void *Reallocate(void *ptr, size_t size) override {
        return nullptr;
    }

    void Free(void *ptr) override {

    }
};

extern "C" void *BaseAllocatorMalloc(size_t size, void *arg) {
    auto allocator = (BaseAllocator *) arg;
    return allocator->Allocate(size);
}

TEST(CBinTest, LEB128) {
    StackAllocator<> allocator{};
    cbin_writer_t writer{};
    cbin_writer_init(&writer, BaseAllocatorMalloc, nullptr, nullptr, &allocator);

    cbin_write_leb128(&writer, std::numeric_limits<uint64_t>::max());

    size_t written = 0;
    auto buffer = cbin_writer_get_buffer(&writer, &written);
    cbin_reader_t reader{};
    cbin_reader_init(&reader, buffer, written);

    EXPECT_EQ(cbin_read_leb128(&reader, nullptr), std::numeric_limits<uint64_t>::max());
}

TEST(CBinTest, Integers) {
    StackAllocator<> allocator{};
    cbin_writer_t writer{};
    cbin_writer_init(&writer, BaseAllocatorMalloc, nullptr, nullptr, &allocator);

    cbin_write_int8(&writer, std::numeric_limits<int8_t>::min());
    cbin_write_uint8(&writer, std::numeric_limits<uint8_t>::max());
    cbin_write_int16(&writer, std::numeric_limits<int16_t>::min());
    cbin_write_uint16(&writer, std::numeric_limits<uint16_t>::max());
    cbin_write_int32(&writer, std::numeric_limits<int32_t>::min());
    cbin_write_uint32(&writer, std::numeric_limits<uint32_t>::max());
    cbin_write_int64(&writer, std::numeric_limits<int64_t>::min());
    cbin_write_uint64(&writer, std::numeric_limits<uint64_t>::max());

    size_t written = 0;
    auto buffer = cbin_writer_get_buffer(&writer, &written);
    cbin_reader_t reader{};
    cbin_reader_init(&reader, buffer, written);
    EXPECT_EQ(cbin_read_int8(&reader), std::numeric_limits<int8_t>::min());
    EXPECT_EQ(cbin_read_uint8(&reader), std::numeric_limits<uint8_t>::max());
    EXPECT_EQ(cbin_read_int16(&reader), std::numeric_limits<int16_t>::min());
    EXPECT_EQ(cbin_read_uint16(&reader), std::numeric_limits<uint16_t>::max());
    EXPECT_EQ(cbin_read_int32(&reader), std::numeric_limits<int32_t>::min());
    EXPECT_EQ(cbin_read_uint32(&reader), std::numeric_limits<uint32_t>::max());
    EXPECT_EQ(cbin_read_int64(&reader), std::numeric_limits<int64_t>::min());
    EXPECT_EQ(cbin_read_uint64(&reader), std::numeric_limits<uint64_t>::max());

    cbin_writer_destroy(&writer);
}

TEST(CBinTest, Floats) {
    StackAllocator<> allocator{};
    cbin_writer_t writer{};
    cbin_writer_init(&writer, BaseAllocatorMalloc, nullptr, nullptr, &allocator);

    cbin_write_float(&writer, std::numeric_limits<float>::min());
    cbin_write_double(&writer, std::numeric_limits<double>::min());
    cbin_write_float(&writer, std::numeric_limits<float>::max());
    cbin_write_double(&writer, std::numeric_limits<double>::max());
    cbin_write_float(&writer, std::numeric_limits<float>::lowest());
    cbin_write_double(&writer, std::numeric_limits<double>::lowest());

    size_t written = 0;
    auto buffer = cbin_writer_get_buffer(&writer, &written);
    cbin_reader_t reader{};
    cbin_reader_init(&reader, buffer, written);
    EXPECT_FLOAT_EQ(cbin_read_float(&reader), std::numeric_limits<float>::min());
    EXPECT_DOUBLE_EQ(cbin_read_double(&reader), std::numeric_limits<double>::min());
    EXPECT_FLOAT_EQ(cbin_read_float(&reader), std::numeric_limits<float>::max());
    EXPECT_DOUBLE_EQ(cbin_read_double(&reader), std::numeric_limits<double>::max());
    EXPECT_FLOAT_EQ(cbin_read_float(&reader), std::numeric_limits<float>::lowest());
    EXPECT_DOUBLE_EQ(cbin_read_double(&reader), std::numeric_limits<double>::lowest());

    cbin_writer_destroy(&writer);
}

TEST(CbinTest, Strings) {
    StackAllocator<> allocator{};
    cbin_writer_t writer{};
    cbin_writer_init(&writer, BaseAllocatorMalloc, nullptr, nullptr, &allocator);

    std::string test_string = "Hello, World!";
    cbin_write_string(&writer, test_string.c_str());
    EXPECT_FALSE(cbin_writer_has_error(&writer));

    size_t written = 0;
    auto buffer = cbin_writer_get_buffer(&writer, &written);
    cbin_reader_t reader{};
    cbin_reader_init(&reader, buffer, written);

    size_t length = 0;
    const auto str = cbin_read_string(&reader, &length);
    std::string_view view(str, length);
    EXPECT_EQ(length, test_string.size());
    EXPECT_EQ(view, test_string);
}

TEST(CBinTest, Errors) {
    cbin_writer_t writer{};
    cbin_writer_init(&writer, nullptr, nullptr, nullptr, nullptr);
    cbin_write_int64(&writer, 123);
    EXPECT_TRUE(cbin_writer_has_error(&writer));
    EXPECT_EQ(cbin_writer_get_error(&writer).error, CBIN_ERROR_OUT_OF_MEMORY);

    StackAllocator<3> allocator{};
    cbin_writer_init(&writer, BaseAllocatorMalloc, nullptr, nullptr, &allocator);
    cbin_write_int64(&writer, 123);
    EXPECT_TRUE(cbin_writer_has_error(&writer));
    EXPECT_EQ(cbin_writer_get_error(&writer).error, CBIN_ERROR_OUT_OF_MEMORY);

    cbin_reader_t reader{};
    cbin_reader_init(&reader, NULL, 0);

    cbin_read_string(&reader, NULL);
    EXPECT_TRUE(cbin_reader_has_error(&reader));
    EXPECT_EQ(cbin_reader_get_error(&reader).error, CBIN_ERROR_END_OF_BUFFER);
}